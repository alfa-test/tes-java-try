package com.example.demo.controller;

import com.example.demo.dto.EmployeeRequestDto;
import com.example.demo.dto.EmployeeResponseDto;
import com.example.demo.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    EmployeeService service;

    @PostMapping()
    public EmployeeResponseDto createEmployee(@RequestBody EmployeeRequestDto requestDto){
        return service.createEmployee(requestDto);
    }

    @GetMapping
    public List<EmployeeResponseDto> getAllEmployee(){
        return service.getAllEmployee();
    }

    @GetMapping("/{id}")
    public Object getEmployeeById(@PathVariable Integer id){

        try {
            return service.getEmployeeById(id);
        } catch (Exception e) {
            e.getMessage();
            return e.getMessage();
        }
    }

    @PutMapping("/{id}")
    public Object updateEmployee(@PathVariable Integer id, @RequestBody EmployeeRequestDto requestDto){
        try {
            return service.updateEmployee(id,requestDto);
        } catch (Exception e) {
            e.getMessage();
            return e.getMessage();
        }
    }

    @DeleteMapping("/{id}")
    public String deleteEmployee(@PathVariable Integer id){

        try {
            return service.deleteEmployeeById(id);
        } catch (Exception e) {
            e.getMessage();
            return e.getMessage();
        }
    }
}
