package com.example.demo.controller;

import com.example.demo.dto.DogsDetailResponseDto;
import com.example.demo.dto.DogsResponseDto;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController()
@RequestMapping("/dogs")
public class DogController {

    RestTemplate restTemplate = new RestTemplate();

    @GetMapping()
    @Cacheable(cacheNames = "all")
    public DogsDetailResponseDto getAllDogsBreeds(){
        return restTemplate.getForEntity("https://dog.ceo/api/breeds/list/all",DogsDetailResponseDto.class).getBody();
    }

    @GetMapping("/{breeds}")
    @Cacheable(value = "detail",key = "#breeds")
    public DogsResponseDto getDogsByBreeds(@PathVariable String breeds){
        return restTemplate.getForEntity("https://dog.ceo/api/breed/"+breeds+"/list",DogsResponseDto.class).getBody();
    }
}
