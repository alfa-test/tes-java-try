package com.example.demo.dto;

import java.io.Serializable;
import java.util.List;

public class DogsResponseDto implements Serializable {

    private List<String> message;

    private String status;

    public List<String> getMessage() {
        return message;
    }

    public void setMessage(List<String> message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
