package com.example.demo.dto;

import java.util.Date;

public class EmployeeResponseDto {

    private Integer id;

    private String fullName;

    private String address;

    private Date dob;

    private Integer roleId;

    private Integer salary;

    public EmployeeResponseDto(Integer id, String fullName, String address, Date dob, Integer roleId, Integer salary) {
        this.id = id;
        this.fullName = fullName;
        this.address = address;
        this.dob = dob;
        this.roleId = roleId;
        this.salary = salary;
    }

    public EmployeeResponseDto() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }
}
