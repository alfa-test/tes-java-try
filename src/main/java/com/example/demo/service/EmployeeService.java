package com.example.demo.service;

import com.example.demo.dto.EmployeeRequestDto;
import com.example.demo.dto.EmployeeResponseDto;

import java.util.List;

public interface EmployeeService {

    EmployeeResponseDto createEmployee(EmployeeRequestDto dto);

    List<EmployeeResponseDto> getAllEmployee();

    EmployeeResponseDto getEmployeeById(Integer id) throws Exception;

    EmployeeResponseDto updateEmployee(Integer id, EmployeeRequestDto dto) throws Exception;

    String deleteEmployeeById(Integer id) throws Exception;


}
