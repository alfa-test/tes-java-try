package com.example.demo.service.impl;

import com.example.demo.dto.EmployeeRequestDto;
import com.example.demo.dto.EmployeeResponseDto;
import com.example.demo.model.Employee;
import com.example.demo.model.Salary;
import com.example.demo.repository.EmployeeRepository;
import com.example.demo.repository.RoleRepository;
import com.example.demo.repository.SalaryRepository;
import com.example.demo.service.EmployeeService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    SalaryRepository salaryRepository;

    @Override
    @Transactional
    public EmployeeResponseDto createEmployee(EmployeeRequestDto dto) {

        Employee employee = new Employee();
        if (null!=dto.getFullName())
            employee.setFullName(dto.getFullName());
        if (null!=dto.getAddress())
            employee.setAddress(dto.getAddress());
        if (null!=dto.getDob())
            employee.setDob(dto.getDob());
        if (null!=dto.getRoleId())
            employee.setRole(roleRepository.getOne(dto.getRoleId()));
        employeeRepository.save(employee);

        Salary salary = new Salary();
        if (null!=dto.getSalary())
            salary.setSalary(dto.getSalary());
        salaryRepository.save(salary);

        Employee savedEmployee = employeeRepository.findByFullNameAndAddressAndDobAndRole_Id(dto.getFullName(),dto.getAddress(),dto.getDob(),dto.getRoleId());
        Salary savedSalary = salaryRepository.getOne(savedEmployee.getId());

        return new EmployeeResponseDto(savedEmployee.getId(),savedEmployee.getFullName(),savedEmployee.getAddress(),savedEmployee.getDob(),savedEmployee.getRole().getId(),savedSalary.getSalary()) ;
    }

    @Override
    public List<EmployeeResponseDto>  getAllEmployee() {

        List<Employee> employeeList = employeeRepository.findAll();
        List<EmployeeResponseDto> employeeResponseDtoList = new ArrayList<>();
        for (Employee employee : employeeList) {
            EmployeeResponseDto saveEmployee = new EmployeeResponseDto();
            saveEmployee.setId(employee.getId());
            saveEmployee.setFullName(employee.getFullName());
            saveEmployee.setAddress(employee.getAddress());
            saveEmployee.setDob(employee.getDob());
            saveEmployee.setRoleId(employee.getRole().getId());
            Salary salary = salaryRepository.getOne(employee.getId());
            saveEmployee.setSalary(salary.getSalary());
            employeeResponseDtoList.add(saveEmployee);
        }

        return employeeResponseDtoList;
    }

    @Override
    public EmployeeResponseDto getEmployeeById(Integer id) throws Exception {

        Employee employee = employeeRepository.getOne(id);
        Salary salary = salaryRepository.getOne(id);

        if (null!=employee)
            return new EmployeeResponseDto(employee.getId(),employee.getFullName(),employee.getAddress(),employee.getDob(),employee.getRole().getId(),salary.getSalary()) ;
        else
            throw new Exception("Employee with id "+id+" not found!");
    }

    @Override
    @Transactional
    public EmployeeResponseDto updateEmployee(Integer id, EmployeeRequestDto dto) throws Exception {

        Employee employee = employeeRepository.getOne(id);
        if (null!=employee) {
            if (null != dto.getFullName())
                employee.setFullName(dto.getFullName());
            if (null != dto.getAddress())
                employee.setAddress(dto.getAddress());
            if (null != dto.getDob())
                employee.setDob(dto.getDob());
            if (null != dto.getRoleId())
                employee.setRole(roleRepository.getOne(dto.getRoleId()));
            employeeRepository.save(employee);

            Salary salary = salaryRepository.getOne(id);
            if (null != dto.getSalary())
                salary.setSalary(dto.getSalary());
            salaryRepository.save(salary);
            return new EmployeeResponseDto(id, employee.getFullName(),employee.getAddress(),employee.getDob(),employee.getRole().getId(),salary.getSalary());

        }else
            throw new Exception("Employee with id "+id+" not found!");
    }

    @Override
    public String deleteEmployeeById(Integer id) throws Exception {

        Employee employee = employeeRepository.getOne(id);
        if (null!= employee){
            salaryRepository.deleteById(employeeRepository.getOne(id).getId());
            employeeRepository.deleteById(id);

            return "Employee with Id " + id + " have been deleted succesfully";
        }else
            throw new Exception("Employee with id "+id+" not found!");
    }
}
