package com.example.demo.repository;

import com.example.demo.model.Employee;
import org.springframework.data.annotation.ReadOnlyProperty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

    public Employee findByFullNameAndAddressAndDobAndRole_Id(String fullName, String address, Date dob, Integer roleId);
}
